// Enable vulkan related definitions in the openxr header, e.g. XR_KHR_VULKAN_ENABLE2_EXTENSION_NAME
#include <vulkan/vulkan_core.h>
#define XR_USE_GRAPHICS_API_VULKAN
#include "openxr/openxr_platform.h"

#include <loader_interfaces.h>

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

// xrCreateInstance passes an opaque handle to the application.
// This handle encapsulates an internal state of the XrInstance that is not accessible by the application.
struct _XrInstance {
  // remember which extensions the application has enabled
  struct {
    bool vulkan2;
  } enabled_extensions;

  char application_name[XR_MAX_APPLICATION_NAME_SIZE];
};

// This example supports one instance at a time.
// The handle passed to the application in xrCreateInstance will be this global pointer.
// A second call to xrCreateInstance will explicitly be made to fail.
// If multiple instances should be supported, consider using a list/hashmap.
static struct _XrInstance *internal_instance_global = NULL;


// The XrInstance handle is either a "struct XrInstance_T *" typedef or an uint64_t depending on 64 or 32 bit architecture.
// Here we use a 1:1 pointer to our internal XrInstance representation as handle, but we could just as well store our
// internal `struct _XrInstance`s in an array and use an array index as handle, as long as we can find our own
// `struct _XrInstance` from a handle.
static XrInstance
internal_instance_to_handle(struct _XrInstance *instance) {
  return (XrInstance) instance;
}
static struct _XrInstance *
handle_to_internal_instance(XrInstance instance) {
  return (struct _XrInstance *) instance;
}

// all the function definitions are manually converted directly from the typedefs in openxr.h or loader_interfaces.h

static XrResult XRAPI_PTR
_xrEnumerateInstanceExtensionProperties(const char* layerName,
                                        uint32_t propertyCapacityInput,
                                        uint32_t* propertyCountOutput,
                                        XrExtensionProperties* properties)
{
  printf("xrEnumerateInstanceExtensionProperties called with layerName %s, capacity %d\n", layerName, propertyCapacityInput);

  // two call idiom:
  // The application first calls with capacity == 0: we return the desired capacity and do nothing else.
  // Then the application calls with properties == an array of size of at least desired capacity.

  // runtimes must support at least one graphics API extension to be useful.
  // we "support" only the vulkan2 graphics API extension.
  *propertyCountOutput = 1;

  if (propertyCapacityInput == 0) {
    return XR_SUCCESS;
  }

  if (propertyCapacityInput >= 1) {
    snprintf(properties[0].extensionName, XR_MAX_EXTENSION_NAME_SIZE, XR_KHR_VULKAN_ENABLE2_EXTENSION_NAME);
    // current revision is 2 https://www.khronos.org/registry/OpenXR/specs/1.0/html/xrspec.html#XR_KHR_vulkan_enable2
    properties[0].extensionVersion = 2;
  }

  return XR_SUCCESS;
}

static XrResult XRAPI_PTR
_xrCreateInstance(const XrInstanceCreateInfo* createInfo,
                  XrInstance* instance)
{
  printf("xrCreateInstance called with application name %s for api version %d.%d.%d engine %s %d\n",
         createInfo->applicationInfo.applicationName,
         XR_VERSION_MAJOR(createInfo->applicationInfo.apiVersion),
         XR_VERSION_MINOR(createInfo->applicationInfo.apiVersion),
         XR_VERSION_PATCH(createInfo->applicationInfo.apiVersion),
         createInfo->applicationInfo.engineName,
         createInfo->applicationInfo.engineVersion);

  if (internal_instance_global != NULL) {
    printf("This runtime only supports a single xrInstance\n");
    return XR_ERROR_RUNTIME_FAILURE;
  }

  internal_instance_global = calloc(1, sizeof(struct _XrInstance));
  printf("Allocated instance pointer %p\n", internal_instance_global);

  memcpy(internal_instance_global->application_name, createInfo->applicationInfo.applicationName, XR_MAX_APPLICATION_NAME_SIZE * sizeof(char));
  printf("Application enabled %d extensions:\n", createInfo->enabledExtensionCount);
  for (uint32_t i = 0; i < createInfo->enabledExtensionCount; i++) {
    printf("\t%s\n", createInfo->enabledExtensionNames[i]);

    if (strcmp(createInfo->enabledExtensionNames[i], XR_KHR_VULKAN_ENABLE2_EXTENSION_NAME) == 0) {
      internal_instance_global->enabled_extensions.vulkan2 = true;
    }
  }


  *instance = internal_instance_to_handle(internal_instance_global);
  printf("Return instance handle: %p\n", *instance);

  return XR_SUCCESS;
}


static XrResult XRAPI_PTR
_xrGetInstanceProperties(XrInstance instance,
                         XrInstanceProperties* instanceProperties)
{
  printf("xrGetInstanceProperties called with instance handle %p\n", instance);

  struct _XrInstance *internal_instance = handle_to_internal_instance(instance);

  snprintf(instanceProperties->runtimeName, XR_MAX_RUNTIME_NAME_SIZE, "sample runtime");
  instanceProperties->runtimeVersion = XR_MAKE_VERSION(0, 0, 1);

  return XR_SUCCESS;
}


// this function is called by the loader and runtimes to call our implementations of OpenXR functions
static XrResult XRAPI_PTR
_xrGetInstanceProcAddr(XrInstance instance,
                       const char* name,
                       PFN_xrVoidFunction* function)
{
  printf("xrGetInstanceProcAddr called with instance handle %p name %s\n", instance, name);

  if (strcmp(name, "xrEnumerateInstanceExtensionProperties") == 0) {
    *function = (PFN_xrVoidFunction)_xrEnumerateInstanceExtensionProperties;
    return XR_SUCCESS;
  }

  if (strcmp(name, "xrCreateInstance") == 0) {
    *function = (PFN_xrVoidFunction)_xrCreateInstance;
    return XR_SUCCESS;
  }

  if (strcmp(name, "xrGetInstanceProperties") == 0) {
    *function = (PFN_xrVoidFunction)_xrGetInstanceProperties;
    return XR_SUCCESS;
  }

  return XR_ERROR_FUNCTION_UNSUPPORTED;
}

// This is the only "exported" function of our runtime that is directly callable.
// All other functions are only called through function pointers that are acquired through
// runtimeRequest->getInstanceProcAddr, which points to our _xrGetInstanceProcAddr implementation.
XrResult XRAPI_PTR
xrNegotiateLoaderRuntimeInterface(const XrNegotiateLoaderInfo *loaderInfo,
                                  XrNegotiateRuntimeRequest *runtimeRequest)
{
  // validating runtimeRequest->structType == XR_LOADER_INTERFACE_STRUCT_RUNTIME_REQUEST etc. might be a good idea to make sure we don't work with garbage data if something is wrong.

  printf("xrNegotiateLoaderRuntimeInterface called with minInterfaceVersion %d, maxInterfaceVersion %d, minApiVersion %d.%d.%d, maxApiVersion %d.%d.%d\n",
         loaderInfo->minInterfaceVersion,
         loaderInfo->maxInterfaceVersion,
         XR_VERSION_MAJOR(loaderInfo->minApiVersion),
         XR_VERSION_MINOR(loaderInfo->minApiVersion),
         XR_VERSION_PATCH(loaderInfo->minApiVersion),
         XR_VERSION_MAJOR(loaderInfo->maxApiVersion),
         XR_VERSION_MINOR(loaderInfo->maxApiVersion),
         XR_VERSION_PATCH(loaderInfo->maxApiVersion));


  runtimeRequest->getInstanceProcAddr = _xrGetInstanceProcAddr;

  // If the headers in openxr_headers/ are updated, these versions might change and require code updates.
  runtimeRequest->runtimeApiVersion = XR_CURRENT_API_VERSION;
  runtimeRequest->runtimeInterfaceVersion = XR_CURRENT_LOADER_RUNTIME_VERSION;

  return XR_SUCCESS;
}
