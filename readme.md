# OpenXR runtime template

This project contains very incomplete sample code implementing an OpenXR runtime. It is intended to demonstrate the the very basic principles of how OpenXR runtimes operate.

Hopefully it is understood that the words "OpenXR runtime" are used in this project only for illustration as this is a sample project and not a serious attempt at implementing a runtime. It is not conformant and does not claim to implement the OpenXR API.

If you want to implement an OpenXR runtime and call it an OpenXR runtime, you must pass the conformance test suite, and submit its results to Khronos, along with paying the associated fee (open source projects *may* be eligible for having the fee waived - talk with Khronos about it). See the [OpenXR-CTS readme](https://github.com/KhronosGroup/OpenXR-CTS) for details.

# Status

This runtime implements only few functions:

* xrEnumerateInstanceExtensionProperties (indicating XR_KHR_vulkan_enable2 as supported extension)
* xrCreateInstance
* xrGetInstanceProperties

Nothing useful can be done with this limited set of functions except listing the supported extensions and properties of a runtime.

For example `hello_xr -G Vulkan2` will do exactly that. It will get to printing
```
[17:20:48.176][Info   ] Instance RuntimeName=sample runtime RuntimeVersion=0.0.1
```
and then crash because `xrGetSystem` is not implemented.

This sample code keeps it simple and only shows how to implement the client library part of an OpenXR runtime and has no long-running server like SteamVR (vrmonitor/vrcompositor) or Oculus Home. Instead the life cycle of the entire OpenXR runtime is tied to a single OpenXR application: When an OpenXR application is started, the runtime is loaded like any other shared library and runs completely in the process space of the OpenXR application. It could start thread to open and continuously poll usb devices of a VR headset when the application calls xrCreateInstance or perhaps xrGetSystem. It could start another thread to run a VR compositor when the application calls xrBeginSession etc.

Writing a "client-only" OpenXR runtime can be completely valid, though it can have some drawbacks - running a second OpenXR application would for example fail to open the usb devices again, unless the runtime has system-level functionality to cooperate with other instances of the same runtime running. Following this train of thought will lead to the creation of a long running server that each instance of the runtime client can connect to - things like opening and polling usb devices and rendering frames to the headset would be conveniently done in this long running server. Implementing such a system is left as an exercise to the reader.

# Building and Running

The only dependency is Vulkan (loader, headers).

Build the runtime with meson:

    meson build
    ninja -C build

On Linux an OpenXR application like `hello_xr` can then be started with this runtime using the included `openxr_runtime_template_linux.json`:

```
XR_RUNTIME_JSON=$PWD/openxr_runtime_template_linux.json hello_xr -G Vulkan2
```

Here we use `XR_RUNTIME_JSON` to point to the included json file. To set our runtime as default we need to use a platform specific way to set it as the "active runtime". On Linux this is done with a symlink into one of the respective xdg config directories: `ln -sf $PWD/openxr_runtime_template_linux.json ~/.config/openxr/1/active_runtime.json`. A full fledged runtime usually provides a tool or installation option to automate this process. For more information refer to [the loader docs](https://www.khronos.org/registry/OpenXR/specs/1.0/loader.html#runtime-discovery).

Note that the relative library_path `./build/libruntime_client.so` starts with a dot, which denotes that the library is searched relative to the location of the `openxr_runtime_template_linux.json` manifest file. It is a good idea to generate an updated manifest file during compilation or installation with a new relative or absolute library_path.
